package com.zzhua.pojo;

import lombok.Data;

/**
 * @description
 * @Author: zzhua
 * @Date 2023/1/28
 */
@Data
public class User {

    private String userAccount;

}
