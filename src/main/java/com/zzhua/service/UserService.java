package com.zzhua.service;

import com.zzhua.dao.mapper.UserMapper;
import com.zzhua.dao.mapper2.UserMapper2;
import com.zzhua.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

@Service
@EnableAspectJAutoProxy(exposeProxy = true)
@EnableTransactionManagement
public class UserService {

    @Autowired
    private UserMapper userMapper; // 数据库user

    @Autowired
    private UserMapper2 userMapper2; // 数据库user2

    @Autowired
    private PlatformTransactionManager transactionManager1;

    @Autowired
    private PlatformTransactionManager transactionManager2;

    @Transactional(transactionManager = "transactionManager1")
    public void addUser() {

        addUserTx();
    }

    @Transactional(transactionManager = "transactionManager2")
    public void addUserTx() {

        User user1 = new User();
        user1.setUserAccount("user1");
        userMapper.addUser(user1);

        User user2 = new User();
        user2.setUserAccount("user2");
        userMapper2.addUser(user2);

        int i = 1 / 0;


    }

}
