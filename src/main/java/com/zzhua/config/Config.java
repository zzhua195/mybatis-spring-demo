package com.zzhua.config;

import com.zzhua.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Import({MybatisConfig1.class,MybatisConfig2.class, UserService.class})
public class Config {

    @Autowired
    private DataSource dataSource1;

    @Autowired
    private DataSource dataSource2;

    @Bean
    public PlatformTransactionManager transactionManager1(DataSource dataSource1) {
        return new DataSourceTransactionManager(dataSource1);
    }

    @Bean
    public PlatformTransactionManager transactionManager2(DataSource dataSource2) {
        return new DataSourceTransactionManager(dataSource2);
    }

}
