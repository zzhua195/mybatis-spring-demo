package com.zzhua.dao.mapper;

import com.zzhua.pojo.Person;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PersonMapper {

    @Insert("insert into person values(#{name} )")
    void addPerson(String name);

    @Select("select * from person")
    List<Person> getPersons();

}
