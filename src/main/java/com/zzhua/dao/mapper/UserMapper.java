package com.zzhua.dao.mapper;

import com.zzhua.pojo.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper {

    List<User> findAll();
    int addUser(User user);

    @Select("select * from `user` u where u.user_account = #{userAccount}")
    User findOne(String userAccount);

}
