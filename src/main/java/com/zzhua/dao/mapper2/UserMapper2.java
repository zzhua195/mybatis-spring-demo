package com.zzhua.dao.mapper2;

import com.zzhua.pojo.User;
import com.zzhua.pojo.User2;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper2 {

    List<User2> findAll2();

    @Select("select * from `user2` u where u.user_account = #{userAccount}")
    User2 findOne2(String userAccount);

    int addUser(User user2);
}
