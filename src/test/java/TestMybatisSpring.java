import com.zzhua.config.Config;
import com.zzhua.service.UserService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @description
 * @Author: zzhua
 * @Date 2023/1/28
 */

public class TestMybatisSpring {
    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);


        UserService userService = context.getBean(UserService.class);

        userService.addUser();

        System.out.println();


    }
}
